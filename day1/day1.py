
def main():
    # open the input file
    f = open("input","r")
    
    # store the max calorie count.
    maxCalorieCount = 0

    # read all lines. 
    # we need to read all of the lines and split based on the presence of two line breaks
    # because the last elf *doesnt* have a line break AFTER it.
    lines = f.read()
    calories = lines.strip().split('\n\n')

    for elf in calories:
        elfCalories = sum(map(int,elf.split('\n')))
        if elfCalories > maxCalorieCount:
            maxCalorieCount = elfCalories

    # now print the stats
    print(f'{maxCalorieCount} is the max calorie count.')
    
if __name__=="__main__":
    main()