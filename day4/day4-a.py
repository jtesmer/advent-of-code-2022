"""
--- Part Two ---

It seems like there is still quite a bit of duplicate work planned. Instead, the Elves would like to know the number of pairs that overlap at all.

In the above example, the first two pairs (2-4,6-8 and 2-3,4-5) don't overlap, while the remaining four pairs (5-7,7-9, 2-8,3-7, 6-6,4-6, and 2-6,4-8) do overlap:

    5-7,7-9 overlaps in a single section, 7.
    2-8,3-7 overlaps all of the sections 3 through 7.
    6-6,4-6 overlaps in a single section, 6.
    2-6,4-8 overlaps in sections 4, 5, and 6.

So, in this example, the number of overlapping assignment pairs is 4.

In how many assignment pairs do the ranges overlap?

"""

"""
    Example: 2-4,6-8
    scale:      1........10
                |        |
    coverage 1: -|-|------
    coverage 2: -----|-|--
        result: no overlap.
    2-6=-4, 4-8=-4: both negative, no overlap

    Example: 2-8,3-7
    coverage 1: -|-----|--
    coverage 2: --|---|---
    2-3=-2,8-8=1: one negative, overlap
    result: 2 is in 1.

    Example: 3-8,3-7
    coverage 1: --|----|--
    coverage 2: --|---|---
    result: 2 is in 1.
    3-3=0,8-7=1, one zer0, one positive, overlap

    Example: 4-6,3-7
    coverage 1: ---|-|----
    coverage 2: --|---|---
    result: 1 is in 2
    4-3=1,6-7=-1, one negative, overlap

    Example: 4-6,5-8
    coverage 1: ---|-|----
    coverage 2: ----|--|--
    result: overlap, but no containment
    4-5=-1, 6-8=-2, both negative

    Example: 5-8,4-6
    coverage 1: ----|--|--
    coverage 2: ---|-|----
    result: overlap, but no containment
    5-4=1, 8-6=2, both positive

Try counter:
1st 836 = too low
2nd 914 = correct!
"""
def evaluateCoverage(coveragePair):
    coverages = coveragePair.strip().split(",")

    c1Range = coverages[0].split("-")
    c2Range = coverages[1].split("-")
    
    if (int(c1Range[1])<int(c2Range[0]))|(int(c1Range[0])>int(c2Range[1])):
        return False
    else:
        return True
    


    

def main():
    file = open('day4/input','r')
    coveragePairs = file.read().strip().split("\n")
    
    groups = []
    currentGroup = []
    for pair in coveragePairs:
        if len(currentGroup)<2:
            currentGroup.append(pair)
        elif len(currentGroup)==2:
            groups.append(currentGroup)
            currentGroup=[]    
            currentGroup.append(pair)
    
    # this is necessary to clean up after we process the last sack.
    groups.append(currentGroup)

    overlapCounter = 0


    for coveragePair in coveragePairs:
        # evaluate the coverage
        # update the counter.
        if evaluateCoverage(coveragePair):
            overlapCounter+=1

    print(f"Found {overlapCounter} overlaps.")

if __name__=="__main__":
    main()