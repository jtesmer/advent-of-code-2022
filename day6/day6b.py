"""
--- Part Two ---

Your device's communication system is correctly detecting packets, but still isn't working. It looks like it also needs to look for messages.

A start-of-message marker is just like a start-of-packet marker, except it consists of 14 distinct characters rather than 4.

Here are the first positions of start-of-message markers for all of the above examples:

    mjqjpqmgbljsphdztnvjfqwrcgsmlb: first marker after character 19
    bvwbjplbgvbhsrlpgdmjqwftvncz: first marker after character 23
    nppdvjthqldpwncqszvftbrmjlhg: first marker after character 23
    nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg: first marker after character 29
    zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw: first marker after character 26

How many characters need to be processed before the first start-of-message marker is detected?


"""


def uniquemembers(characters):
    for item in characters:
        if characters.count(item)==1:
            continue
        else:
            return False
    return True


def main():

    linecounter = 0
    positioncounter = 0
    foundit = False
    buffer = []

    with open("day6/input", "r") as inputfile:
        line = inputfile.readline()
        while line:
            linecounter+=1
            print(f"{linecounter}: {line}")

            for character in line:
                if len(buffer)<14:
                    buffer.append(character)
                    positioncounter += 1
                elif len(buffer)>=14:
                    if uniquemembers(buffer):
                        # found it
                        print(f"Found token {buffer} at position {positioncounter} on line {linecounter}.")
                        foundit = True
                        break
                    else:
                        print("Token not found yet.")
                        buffer.pop(0)
                        buffer.append(character)
                        positioncounter+=1

            if foundit:
                positioncounter=0
                buffer = []
                foundit=False

            
            line = inputfile.readline()


if __name__=="__main__":
    main()