"""
--- Part Two ---

The Elf finishes helping with the tent and sneaks back over to you. "Anyway, the second column says how the round needs to end: X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win. Good luck!"

The total score is still calculated in the same way, but now you need to figure out what shape to choose so the round ends as indicated. The example above now goes like this:

    In the first round, your opponent will choose Rock (A), and you need the round to end in a draw (Y), so you also choose Rock. This gives you a score of 1 + 3 = 4.
    In the second round, your opponent will choose Paper (B), and you choose Rock so you lose (X) with a score of 1 + 0 = 1.
    In the third round, you will defeat your opponent's Scissors with Rock for a score of 1 + 6 = 7.

Now that you're correctly decrypting the ultra top secret strategy guide, you would get a total score of 12.

Following the Elf's instructions for the second column, what would your total score be if everything goes exactly according to your strategy guide?
"""

scoreMap = {
    "A":1, # rock
    "B":2, # paper
    "C":3, # scissors
    "X":1, # rock
    "Y":2, # paper
    "Z":3  # scissors
}
strategy = {
    "A": {          # rock
        "X":"Z",     # lose
        "Y":"X",     # draw
        "Z":"Y"      # win
    },
    "B": {          # paper
        "X":"X",     # lose
        "Y":"Y",     # draw
        "Z":"Z"      # win
    },
    "C": {          # scissors
        "X":"Y",     # lose
        "Y":"Z",     # draw  
        "Z":"X"      # win
    }
}


def scoreHand(yourHand,myHand):

    yourScore = scoreMap[yourHand]
    myScore = scoreMap[myHand]

    if yourHand=='A': # rock
        if myHand=='Z': # scissors
            yourScore = yourScore + 6
        elif myHand=='Y': # paper
            myScore = myScore + 6
        elif myHand=='X': # rock
            myScore = myScore + 3
            yourScore = yourScore + 3
    elif yourHand=='B': # paper
        if myHand=='Z': # scissors
            myScore = myScore + 6
        elif myHand=='Y': # paper
            myScore = myScore + 3
            yourScore = yourScore + 3
        elif myHand=='X': # rock
            yourScore = yourScore + 6
    elif yourHand=='C': # scissors
        if myHand=='Z': # scissors
            myScore = myScore + 3
            yourScore = yourScore + 3
        elif myHand=='Y': # paper
            yourScore = yourScore + 6
        elif myHand=='X': # rock
            myScore = myScore + 6

    return {"myScore":myScore,"yourScore":yourScore}



def main():

    file = open('day2/input','r')
    currentScore = 0
    roundCounter = 0
    rounds = file.read().strip().split("\n")
    for round in rounds:
        yourHand=round[0]
        myHand=round[2]
        result = scoreHand(yourHand,strategy[yourHand][myHand])
        print(f"Result: your hand: {yourHand} my hand: {myHand} scores: {result['yourScore']}/{result['myScore']}.")
        roundCounter += 1
        currentScore += result["myScore"]

    print(f"Your score is {currentScore:,} after {roundCounter:,} rounds.")


if __name__=="__main__":
    main()
