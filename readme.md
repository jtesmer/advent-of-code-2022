# Advent of Code 2022
This is my first Advent of Code. How fun!

# What is Advent of Code?
See https://adventofcode.com/2022

# Who am I?
I'm John G Tesmer, COO of [IQNOX](https://www.iqnox.com)

# What did I learn?

# Try Counter
|Part|Tries|
|--|--:|
|1|2|
|2|1|
|3a|1|
|3b|2|
|4a|1|
|4b|2|
|5a|1|
|5b|1|
|6a|1|
|6b|1|

## Day 1
* A refresher on Python, including how to call the main function automatically.
* To check my assumptions! I assumed that the file ended with an empty line, but in reality it just *ended*, which meant that the last elf's snacks were not included in my total. Quite a genius move by the authors of the challenge, for sure, because it only broke my logic on part 2.

## Day 2
* I remembered about reading files from day 1. That was a good lesson.
* I liked the second part of day 2, and it made me consider that there is probably a more efficient way to handle this solution than the function I wrote. I experimented with 2 dimensional dictionary for the strategy and that worked pretty well, I think, but I can't comment on its efficiency.
* The main bugs I had in this day were getting lost in the letters. I ended up writing in some comments, but I wonder if there's a better way? Enums?
* I got part 2 of day 2 correct on the first try!

## Day 3
### Part 1
* I got this on the first try!
### Part 2
* My initial attempt at parsing the input file and reading the sacks was too naieve. I had the right idea, but I had to account for the edges. Lesson learned, again, confirm your inputs are being read correctly!
* My initial attempt at identifying the badge for the group was too naieve. I initially counted each of the items across all sacks in the group and looked for the one that had a count of 3. This was actually counting the wrong thing: I was counting items in the group. If an item - say "r" appeared 3 times in one sack, then it would be counted as the "badge" for the group. This was an "aha" moment, because I realized that I needed to summarize the members of the group individually, then summarize the summary for each of the group members into a new item count dict, at which point I could then loop through the dict and find the one with a count of 3. Again, this is relatively naieve, because it doesnt account for the circumstance where there is a group that *doesnt* have a count of 3. But it worked.
* I learned from prior attempts and created a new "test" file, which used the examples from day 3 part 2. I refined my algorithm to get the same output as the example, but using the "test" file rather than the input file. This worked well.
* VS Code Python debugging interface is pretty great. I remembered that I could move about the stack and see the variables in the different parts of the code. Very helpful.

## Day 4
### Part 1
* I got this on the first try, but I think I was just lucky. The algorithm I came up with - taking the difference of the end points and comparing the values - seems ... weird.
* I learned that there's some interesting behavior with logical ands in Python. For example:

`(delta1<=0 & delta2>=0)` is different than `((delta1<=0)&(delta2>=0))`.
### Part 2
* I learned a bit more about Python types. The input file is not automatically parsed into ints; the numbers remain strings, and comparing strings with numbers in them is not a good time. Things started working properly when I converted the strings to ints and compared those.
* I used my test file again successfully!

## Day 5
### Part 1
* I am embarassed to say that I just learned that `&` and `and` are different in python.
* Testing saved the day again! Got it on the first try!
### Part 2
* Turns out I had written the code for part 2 when I wrote part 1, and realized that I misread the instructions when I was testing part 1. I commented out the "reverse" line to determine the values for part 1. When I read the instructions for part 2, I realized I only needed to uncomment the reverse line. Easy peasy.

## Day 6
## Part 1
* I got this right on the first try.
* I discovered a better way to loop through a file.

## Part 2
* This only required tweaking the size of the buffer I was using. Easy!
* I got this right on the first try.