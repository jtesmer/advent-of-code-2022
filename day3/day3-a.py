"""
--- Part Two ---

As you finish identifying the misplaced items, the Elves come to you with another issue.

For safety, the Elves are divided into groups of three. Every Elf carries a badge that identifies their group. For efficiency, within each group of three Elves, the badge is the only item type carried by all three Elves. That is, if a group's badge is item type B, then all three Elves will have item type B somewhere in their rucksack, and at most two of the Elves will be carrying any other item type.

The problem is that someone forgot to put this year's updated authenticity sticker on the badges. All of the badges need to be pulled out of the rucksacks so the new authenticity stickers can be attached.

Additionally, nobody wrote down which item type corresponds to each group's badges. The only way to tell which item type is the right one is by finding the one item type that is common between all three Elves in each group.

Every set of three lines in your list corresponds to a single group, but each group can have a different badge item type. So, in the above example, the first group's rucksacks are the first three lines:

vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg

And the second group's rucksacks are the next three lines:

wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw

In the first group, the only item type that appears in all three rucksacks is lowercase r; this must be their badges. In the second group, their badge item type must be Z.

Priorities for these items must still be found to organize the sticker attachment efforts: here, they are 18 (r) for the first group and 52 (Z) for the second group. The sum of these is 70.

Find the item type that corresponds to the badges of each three-Elf group. What is the sum of the priorities of those item types?

"""

def identifyBadge(group):

    memberItemSummary = []
    memberItemCount = {}

    for member in group:
        memberItems = {}
        for item in member:
            if item in memberItems:
                memberItems[item]+=1
            else:
                memberItems[item]=1
        
        memberItemSummary.append(memberItems)
    
    for group in memberItemSummary:
        for item in group:
            if item in memberItemCount:
                memberItemCount[item]+=1
            else:
                memberItemCount[item]=1
    
    for item in memberItemCount:
        if memberItemCount[item]==3:
            return item



def scoreElements(listToScore):
    score = 0
    for item in listToScore:
        if len(item)==1:
            if ord(item[0])>=97:
                # we remove 96 here because the lower case "A" has priority = 1, but the ascii value of "A" is 97.
                # therefore 96 is the offset between the ascii codes and the priority values.
                score += ord(item[0])-96
            elif ord(item[0])>=65:
                # we do the funky math here because:
                # "A" is ASCII 65, but it's priority 27.
                # the difference between 65 and 27 is 38.
                # this is the offset between the ASCII codes and the priority values
                score += ord(item[0])-38
        
    return score

def main():

    file = open('day3/input','r')
    sacks = file.read().strip().split("\n")
    groups = []
    currentGroup = []
    groupCounter=0
    for sack in sacks:
        if len(currentGroup)<3:
            currentGroup.append(sack)
        elif len(currentGroup)==3:
            groups.append(currentGroup)
            currentGroup=[]    
            currentGroup.append(sack)
    
    # this is necessary to clean up after we process the last sack.
    groups.append(currentGroup)

    badges=[]
    for group in groups:
        badges.append(identifyBadge(group))

    score = scoreElements(badges)

    print(f"Scored badges. Score is {score}")



if __name__=="__main__":
    main()